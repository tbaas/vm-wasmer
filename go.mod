module chainmaker.org/chainmaker/vm-wasmer/v2

go 1.15

require (
	chainmaker.org/chainmaker/chainconf/v2 v2.2.2
	chainmaker.org/chainmaker/common/v2 v2.2.1
	chainmaker.org/chainmaker/logger/v2 v2.2.1
	chainmaker.org/chainmaker/pb-go/v2 v2.2.1
	chainmaker.org/chainmaker/protocol/v2 v2.2.2
	chainmaker.org/chainmaker/store/v2 v2.2.0
	chainmaker.org/chainmaker/utils/v2 v2.2.2
	chainmaker.org/chainmaker/vm/v2 v2.2.1
	github.com/stretchr/testify v1.7.0
)

replace (
	chainmaker.org/chainmaker/vm/v2 => git.chainmaker.org.cn/tbaas/vm/v2 v2.2.3-0.20220608073435-afdf47c679c4
)
